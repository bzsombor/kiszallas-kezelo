var Auto = require('../../models/auto.model');

//auto lekérése details/edit oldalhoz
module.exports = function(){
    return function(req,res){
        Auto.findOne({ rendszam: req.params.rendszam, _user: req.session._id }, (err, auto) => {
            if (!auto) {
                req.flash('errors', 'Nem tartozik hozzád ilyen rendszámú autót!');
                return res.redirect('/autok');
            }
            return res.render('autok/auto_details', { auto: auto, uj: false });
        });
    }
}