var Kiszallas = require('../../models/kiszallas.model');

// Autóhoz szűrt kiszállások megjelenítése
module.exports = function(){
    return (req, res) => {
        Kiszallas.find({ rendszam: req.params.rendszam, _user: req.session._id }, (err, kiszallasok) => {
            res.render('kiszallasok/kiszallasok', { kiszallasok: kiszallasok, rendszamhoz: req.params.rendszam });
        });
    }
}