var Auto = require('../../models/auto.model');

//Autó mentése
module.exports = function(){
    return function(req,res){
        Auto.create({ rendszam: req.body.rendszam, megnevezes: req.body.megnevezes, _user: req.session._id }, (err, auto) => {
            req.flash('info', 'Új autó felvétele megtörtént');
            return res.redirect('/autok');
        });
    }
}