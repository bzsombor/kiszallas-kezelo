//Post paraméter check
module.exports = function(){
    return function(req,res,next){
        if (!req.body.rendszam || !req.body.megnevezes) {
            req.flash('errors', 'Kérlek add meg a rendszámot és a megnevezést is!');
            return res.redirect('back');
        }
        return next();
    }
}