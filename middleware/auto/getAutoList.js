var Auto = require('../../models/auto.model');
var Kiszallas = require('../../models/kiszallas.model');

// Összes hozzámtartozó autó listázása
module.exports  = function(){
    return function (req, res){
        Auto.find({ _user: req.session._id}, (err, autos) => {
            res.render('autok/autok', { autok: autos });
        });
    }
}