var Auto = require('../../models/auto.model');

//Létrehozható e az autó? van e már ilyen rendszámú?
module.exports = function(){
    return function (req, res, next){
        Auto.findOne({ rendszam: req.body.rendszam, _user: req.session._id }, (err, auto) => {
            if (auto) {
                req.flash('errors', 'Már létezik autó ezzel a rendszámmal!');
                return res.redirect('back');
            }else{
                return next();
            }
        });
    }
}