var Auto = require('../../models/auto.model');

//Autó adatai frissítése
module.exports = (req,res) =>{
    Auto.findOneAndUpdate({ rendszam: req.params.rendszam, _user: req.session._id }, { $set: { megnevezes:req.body.megnevezes}},{new: true}, (err, auto) => {
        if (!auto) {
            req.flash('errors', 'Nem találtunk ilyen rendszámú autót!');
            return res.redirect('back');
        }
        req.flash('infos', 'Auto adatok frissítve!');
        return res.redirect(`/autok/${auto.rendszam}`);
    });
}