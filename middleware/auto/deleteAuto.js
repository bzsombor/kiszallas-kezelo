var Auto = require('../../models/auto.model');

//Autó törlése
module.exports = function(){
    return (req, res) => {
        Auto.findOneAndDelete({ rendszam: req.params.rendszam, _user: req.session._id }, (err, doc) => {
            if (!doc)
                req.flash('errors', 'A törlendő autó nem található!');
            req.flash('infos', 'Autó sikeresen törlésre került!');
            return res.redirect('/autok');
        });
    }
}