var Kiszallas = require('../../models/kiszallas.model');
//kiszállás frissítése
module.exports = function(){
    return (req, res) => {
        var idopont = req.body.idopont_date + " " + req.body.idopont_time;
        Kiszallas.findOneAndUpdate({ _id: req.params.id, _user: req.session._id },
            { rendszam: req.body.rendszam, megnevezes: req.body.megnevezes, idopont: Date.parse(idopont)  }, { new: true }, (err, doc) => {
                if (!doc) {
                    req.flash('errors', 'Nem található a frissítendő kiszállás!');
                    return res.redirect('/kiszallasok');
                }
                req.flash('infos','Kiszállás adatai sikeresen frissültek!')
                return res.redirect(`/kiszallasok/${doc._id}`);
            });
    }

}