var Kiszallas = require('../../models/kiszallas.model');
// Kiszállás felvételedithez szükséges body paraméterek meglétének ellenőrzése
module.exports = function(){
    return (req,res,next) => {
        if (!req.body.rendszam || !req.body.idopont_date || !req.body.idopont_time || !req.body.megnevezes){
            req.flash('errors','Kérlek töltsd ki az összes mezőt!');
            return res.redirect('back');
        }
        return next();

    }
}