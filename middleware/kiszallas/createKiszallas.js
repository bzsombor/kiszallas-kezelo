var Kiszallas = require('../../models/kiszallas.model');
//kiszállás létrehozása
module.exports = function() {
    return (req, res) => {
        var idopont = req.body.idopont_date + " " + req.body.idopont_time;
        Kiszallas.create({ rendszam: req.body.rendszam, megnevezes: req.body.megnevezes, idopont: Date.parse(idopont),_user:req.session._id }, (err, kiszallas) => {
            req.flash('infos', 'Kiszállás felvétele megtörtént!');
            return res.redirect('/kiszallasok');
        });
    }
}