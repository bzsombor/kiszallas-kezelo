var Kiszallas = require('../../models/kiszallas.model');
const dateFormat = require('dateformat');
//egy kiszálls lekérdezése, detail/edit-hez + render
module.exports = function(){
    return (req, res) => {
        Kiszallas.findOne({ _id:req.params.id, _user:req.session._id}, (err, kiszallas) => {
            if (!kiszallas) {
                req.flash('errors','Nem tartozik hozzád ilyen kiszállás');
                return res.redirect('/kiszallasok');
            }

            return res.render('kiszallasok/kiszallas_details', { kiszallas: kiszallas, uj: false, dateFormat: dateFormat });
        });
    }
}