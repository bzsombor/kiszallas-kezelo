var Kiszallas = require('../../models/kiszallas.model');
//felhasznához tartozó kiszállások listázása
module.exports = function(){
    return (req, res) => {
        Kiszallas.find({ _user: req.session._id}, (err, kiszallasok) => {
            res.render('kiszallasok/kiszallasok', { kiszallasok: kiszallasok, rendszamhoz: false });
        });
    }
}