var Kiszallas = require('../../models/kiszallas.model');
//kiszállás törlése
module.exports = function(){
    return (req, res) => {
        Kiszallas.findOneAndDelete({ _id: req.params.id, _user: req.session._id }, (err, doc) => {
            if (!doc)
                req.flash('errors', 'A törlendő kiszállás nem található!');
            return res.redirect('/kiszallasok');
        });
    }
}