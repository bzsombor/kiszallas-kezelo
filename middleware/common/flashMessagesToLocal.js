//Flash üzenetek template-eknek átadása
module.exports =  function (req, res, next){
        res.locals.errors = req.flash('errors');
        res.locals.infos = req.flash('infos');
        next();
}
