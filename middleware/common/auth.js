//Bejelentkezés ellenérzése, ha nincs bejelentkeze átirányít (kivétel a login/register oldal)
module.exports =  function (req, res, next){
        if (req.path == '/login' || req.path == "/register") return next();
        if (req.session._id) {
            return next();
        }
        return res.redirect('/login');
}