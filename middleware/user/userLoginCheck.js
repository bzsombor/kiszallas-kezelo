var User = require('../../models/user.model');
var bcrypt = require('bcrypt');

// user bejelentkeztetés
module.exports =  function (req, res){
        if (!req.body.email || !req.body.password) {
            req.flash('errors', 'Kérlek add meg az email címed és jelszavad!');
            return res.redirect('/login');
        }
        User.findOne({ email: req.body.email }, (err, user) => {
            if (!user) {
                req.flash('errors', 'Hibás belépési adatok!');
                return res.redirect('/login');
            }


            bcrypt.compare(req.body.password, user.password, (err, same) => {
                if (same) {
                    req.session._id = user._id;
                    req.flash('infos', 'Sikeres belépés!');
                    return res.redirect('/autok');
                } else {
                    req.flash('errors', 'Hibás belépési adatok!');
                    return res.redirect('/login');
                }
            });
        });
    }