var User = require('../../models/user.model');
var bcrypt = require('bcrypt');

// új felhasználó létrehozása
module.exports = function (req, res) {
        if (!req.body.email || !req.body.password) {
            req.flash('errors', 'Kérlek add meg az email címed és jelszavad!');
            return res.redirect('/register');

        }
        User.findOne({ email: req.body.email }, (err, user) => {
            if (!user) {
                var pw = bcrypt.hashSync(req.body.password, 10);
                User.create({ email: req.body.email, password: pw },(err,user) => {
                    req.session._id = user._id;
                    req.flash('infos','Sikeres regisztráció!');
                    return res.redirect('/autok');
                });                

            } else {
                req.flash('errors', 'Ilyen email címmel már regisztráltak nálunk!');
                return res.redirect('/register');
            }
        });
    }