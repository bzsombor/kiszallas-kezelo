var expect = require('chai').expect;
var checkAutoParamsMW = require('../middleware/auto/checkAutoParams');
var checkKiszallasParamsMW = require('../middleware/kiszallas/checkKiszallasParams');

describe('checkAutoParams middleware', function(){
    it('should redirect back if params missing',function(done){
               
        var req = {
            flash: function (str, str2) { 
                expect(str2).to.eql('Kérlek add meg a rendszámot és a megnevezést is!');
        } };
        var res = {
            redirect: function (str) {
                expect(str).to.eql("back");
         } };
        checkAutoParamsMW(req,res,() => {});        
        return done();
    });

    it('should next if params found', function (done) {

        var req = {
            flash: function (str, str2) {

            }
        };
        var res = {
            redirect: function (str) {

            }
        };
        checkKiszallasParamsMW(req, res, () => {
            var a = "ha ezt meghívta akkor jók vagyunk";
            expect(a).exist();
        });
        return done();
    });



});
describe('checkKiszallasParams middleware', function(){
    it('should redirect back if params missing',function(done){
               
        var req = {
            flash: function (str, str2) { 
                expect(str2).to.eql('Kérlek töltsd ki az összes mezőt!');
        } };
        var res = {
            redirect: function (str) {
                expect(str).to.eql("back");
         } };
        checkKiszallasParamsMW(req,res,() => {});        
        return done();
    });
    it('should next if params found', function (done) {

        var req = {
            flash: function (str, str2) {
                
            }
        };
        var res = {
            redirect: function (str) {
                
            }
        };
        checkKiszallasParamsMW(req, res, () => {
            var a = "ha ezt meghívta akkor jók vagyunk";
            expect(a).exist();
         });
        return done();
    });
});
