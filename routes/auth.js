var userLoginCheckMW = require('../middleware/user/userLoginCheck');
var registerUserMW = require('../middleware/user/registerNewUser');
module.exports = function(app){


    app.get('/login', (req, res) => {
        return res.render('login')
    });
    app.post('/login', userLoginCheckMW);

    app.get('/register', (req, res) => res.render('register'));
    app.post('/register', registerUserMW);

    app.get('/logout', (req, res) => { req.session._id = false; return res.redirect('/login') })
}