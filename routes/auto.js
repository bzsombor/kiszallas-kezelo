var getAutoListMW = require('../middleware/auto/getAutoList')();
var checkNewAutoMW = require('../middleware/auto/checkNewAuto')();
var createNewAutoMW = require('../middleware/auto/createNewAuto')();
var checkAutoParamsMW = require('../middleware/auto/checkAutoParams')();
var getAutoMW = require('../middleware/auto/getAuto')();
var deleteAutoMW = require('../middleware/auto/deleteAuto')();
var getAutoKiszallasokMW = require('../middleware/auto/getAutoKiszallasok')();
var updateAutoMW = require('../middleware/auto/updateAuto');

module.exports = function(app){

    app.get('/autok', getAutoListMW);
    app.post('/autok', checkAutoParamsMW, checkNewAutoMW, createNewAutoMW);


    app.get('/autok/uj', (req, res) => {

        res.render('autok/auto_details', { auto: { rendszam: "", megnevezes: "" }, uj: true });
    });

    app.get('/autok/:rendszam',getAutoMW);


    app.post('/autok/:rendszam', checkAutoParamsMW, updateAutoMW);


    app.get('/autok/:rendszam/torol', deleteAutoMW);


    app.get('/autok/:rendszam/kiszallasok', getAutoKiszallasokMW);

}