var getKiszallasokMW = require('../middleware/kiszallas/getKiszallasok')();
var getKiszallasMW = require('../middleware/kiszallas/getKiszallas')();
var checkKiszallasParams = require('../middleware/kiszallas/checkKiszallasParams')();
var createKiszallasMW = require('../middleware/kiszallas/createKiszallas')();
var updateKiszallasMW = require('../middleware/kiszallas/updateKiszallas')();
var deleteKiszallasMW = require('../middleware/kiszallas/deleteKiszallas')();
const dateFormat = require('dateformat');
module.exports = function(app){

    app.get('/kiszallasok',getKiszallasokMW );

    app.post('/kiszallasok', checkKiszallasParams,createKiszallasMW);
    app.post('/kiszallasok/:id', checkKiszallasParams,updateKiszallasMW);

    app.get('/kiszallasok/:id/torol', deleteKiszallasMW);


    app.get('/kiszallasok/uj', (req, res) => {
        return res.render('kiszallasok/kiszallas_details', { kiszallas: { rendszam: '', megnevezes: '', idopont: new Date() }, uj: true, dateFormat: dateFormat });
    });

    app.get('/kiszallasok/:id', getKiszallasMW);

}

