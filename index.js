const express = require('express');

const path = require('path');
var app = express();
const config = require('./config')();
const session = require('express-session');
const flash = require('connect-flash');

app.use(session({secret:'topsecretrandomstringusedforencodingandstuffiguess'}));
var bodyParser = require('body-parser')
app.use(bodyParser.json());       //for postman testing
app.use(bodyParser.urlencoded({     
    extended: false
})); 

// app.use(express.json());
// app.use(express.urlencoded({extended: true}));

app.use(express.static(path.join(__dirname, 'public')));
app.use(flash());
app.set('view engine','ejs');

// mongoose.connect(`mongodb://localhost:${db_port}/kiszallas`, { autoIndex: false, useNewUrlParser: true, useFindAndModify: false });





var flashToLocalsMW = require('./middleware/common/flashMessagesToLocal');
var auth = require('./middleware/common/auth');

app.use(flashToLocalsMW);
app.use(auth);

require('./routes/base')(app);
require('./routes/auth')(app);
require('./routes/auto')(app);
require('./routes/kiszallas')(app);


app.listen(config.port, () => console.log(`Example app listening on port ${config.port}!`));