var Schema = require('mongoose').Schema;
var db = require('../db/db_conn');

var Auto = db.model('Auto', {
    rendszam: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    megnevezes: {
        type: String,
        required: true,
    },
    _user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
});

module.exports = Auto;
