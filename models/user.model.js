var db = require('../db/db_conn');


var User = db.model('Felhasznalo', {
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
    }
});
module.exports = User;