var Schema = require('mongoose').Schema;
var db = require('../db/db_conn');


var Kiszallas = db.model('Kiszallas', {
    rendszam: {
        type: String,
        required: true,
        trim: true
    },
    megnevezes: {
        type: String,
        required: true,
    },
    idopont: {
        type: Date,
        required: true,
    },
    _user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
});

module.exports = Kiszallas;