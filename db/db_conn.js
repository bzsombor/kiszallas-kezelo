var config = require('../config.js')();
var mongoose = require('mongoose');
mongoose.connect(`${config.mongodb}/uj4i8b`, { autoIndex: false, useNewUrlParser: true, useFindAndModify: false });
module.exports = mongoose;